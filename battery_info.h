#pragma once

/*! Retrieve the battery data

   Using the OSX libraries get the battery Volatage(in mV), Current(in mA)
   and Temprature(in cC)
*/
void get_battery_data(int * Voltage_mV, int * Current_mA, int * Temp_cC);
