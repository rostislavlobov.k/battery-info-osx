#include <IOKit/IOKitLib.h>
#include <CoreFoundation/CoreFoundation.h>


void get_battery_data(int * Voltage_mV, int * Current_mA, int * Temp_cC) {
	io_service_t batteryService = IOServiceGetMatchingService(kIOMainPortDefault, IOServiceMatching("AppleSmartBattery"));
	CFMutableDictionaryRef batteryProperties;
	IORegistryEntryCreateCFProperties(batteryService, &batteryProperties, kCFAllocatorDefault, 0);

	CFNumberRef voltage = CFDictionaryGetValue(batteryProperties, CFSTR("Voltage"));
	CFNumberGetValue(voltage, kCFNumberIntType, Voltage_mV);

	CFNumberRef current = CFDictionaryGetValue(batteryProperties, CFSTR("Amperage"));
	int currentValue = 0;
	CFNumberGetValue(current, kCFNumberIntType, Current_mA);
	// The returned value is signed integer so in case current is negative we want to cast it,
	// also when discharging by convention value should be positive but it's negative so we invert the sign.
	*Current_mA = ((int)*Current_mA) * -1;

	CFNumberRef temp = CFDictionaryGetValue(batteryProperties, CFSTR("VirtualTemperature"));
	CFNumberGetValue(temp, kCFNumberIntType, Temp_cC);

	IOObjectRelease(batteryService);
}
