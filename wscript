#!/usr/bin/env python
# encoding: utf-8

def options(opt):
	opt.load('compiler_c gnu_dirs')

def configure(conf):
	conf.load('compiler_c gnu_dirs')

def build(bld):
	bld(
	 target="battery_info",
	 features="c cshlib",
	 source = "battery_info.c",
	 linkflags=[
	 '-framework', 'IOKit',
	 '-framework', 'CoreFoundation'
	 ],
	 vnum="0.1",
	)
